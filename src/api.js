// import axios from "axios";

// import addOAuthInterceptor from "axios-oauth-1.0a";

// class API {
//     constructor(){
//         this.base_url = "http://localhost/react-word/wp-json/wc/v3"
//     }
//     async saveOrder(data){
//         const client =   axios.create();
//         addOAuthInterceptor(client, {
//             key: "ck_6c95f1f5f5e765044ec1edbe3fd1709837bc3cac",
//             secret: "cs_1d77c90b87e8cc8c32b97ee03154cc3c5f0918fa",
//            algorithm: "HMAC-SHA1",
//         }); 
//         let set = {
//             'payment_method' : 'bacs',
//             'payment_method_title' : 'Direct Bank Transfer',
//             'set_paid' : data.payment,
//             'billing' : {
//                 'first_name' : data.b_f_name,
//                 'last_name' : data.b_l_name,
//                 'address_1' : data.b_a_l_1,
//                 'address_2' : data.b_l_2,
//                 'city' : data.b_city,
//                 'state' : data.b_state,
//                 'postcode' : data.b_post_code,
//                 'country' : data.b_country,
//                 'email' : data.b_email,
//                 'phone' : data.b_mobile
//             },
//             'shipping' : {
//                 'first_name' : data.s_f_name,
//                 'last_name' : data.s_l_name,
//                 'address_1' : data.s_a_l_1,
//                 'address_2' : data.s_l_2,
//                 'city' : data.s_city,
//                 'state' : data.s_state,
//                 'postcode' : data.s_post_code,
//                 'country' : data.c_country,
//             },
//             'line_items' : data.items,
//             'shipping_lines' : [
//                 {
//                     'method_id' : 'flat_rate',
//                     'method_title' : 'Flat Rate',
//                     'total' : data.shipping_charges
//                 }
//             ]
//         };
//         const res = client.post(this.base_url + '/orders',set)
//         return res.data
//     }
//     async getOrders(){
//         const client = axios.create();
//         addOAuthInterceptor(client, {
//             key: "ck_6c95f1f5f5e765044ec1edbe3fd1709837bc3cac",
//             secret: "cs_1d77c90b87e8cc8c32b97ee03154cc3c5f0918fa",
//            algorithm: "HMAC-SHA1",
//         }); 
//         const res = await client.get(this.base_url + '/orders')
//         return res.data
//     }
//     async getCustomers(){
//         const client = axios.create();
//         addOAuthInterceptor(client, {
//             key: "ck_6c95f1f5f5e765044ec1edbe3fd1709837bc3cac",
//             secret: "cs_1d77c90b87e8cc8c32b97ee03154cc3c5f0918fa",
//            algorithm: "HMAC-SHA1",
//         }); 
//         const res = await client.get(this.base_url + '/customers')
//         return res.data
//     }
//     async getProducts(){
//         const client = axios.create();
//         addOAuthInterceptor(client, {
//             key: "ck_6c95f1f5f5e765044ec1edbe3fd1709837bc3cac",
//             secret: "cs_1d77c90b87e8cc8c32b97ee03154cc3c5f0918fa",
//            algorithm: "HMAC-SHA1",
//         }); 
//         const res = await client.get(this.base_url + '/products')
//         return res.data
//     }

// }
// export default (new API)
