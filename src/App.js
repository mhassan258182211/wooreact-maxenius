import React, {
    useReducer,
    createContext,
    useContext,
    useState,
    useEffect,
} from "react";
  import "./App.css";

import MyContext from "./components/contextApi/context";
import ReactDOM from "react-dom";
import { createBrowserHistory } from "history";
import { Router, Route, Switch, Redirect } from "react-router-dom";
// core components
import Admin from "layouts/Admin.js";
import RTL from "layouts/RTL.js";
import "assets/css/material-dashboard-react.css?v=1.9.0";
import Products from "views/Products/Products";
import { object } from "prop-types";

const axios = require("axios");
const hist = createBrowserHistory();

function App() {


    const [products, setproducts] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [orders, setOrders] = useState();
    const [customerdetail, setCustomerDetail] = useState({});
    // const [active, setActive] = useState();
    // const [recovered, setRecovered] = useState();
    // const [loading, setLoading] = useState(true);
    // const [dataCity, setDataCity] = useState("Gloable");

    var WooCommerceAPI = require('woocommerce-api');

        var WooCommerce = new WooCommerceAPI({
            url: 'https://capinfostaging.wpengine.com/',
            consumerKey: 'ck_6ddb1708df95f8007518d2a08e5dcef14fc7e2e2',
            consumerSecret: 'cs_9297a433ce9039451dfd3417bd0c70c5c4770e96',
            wpAPI: true,
            version: 'wc/v1',
            queryStringAuth: true
        });

    useEffect(() => {

        var WooCommerceAPI = require('woocommerce-api');

        var WooCommerce = new WooCommerceAPI({
            url: 'https://capinfostaging.wpengine.com/',
            consumerKey: 'ck_6ddb1708df95f8007518d2a08e5dcef14fc7e2e2',
            consumerSecret: 'cs_9297a433ce9039451dfd3417bd0c70c5c4770e96',
            wpAPI: true,
            version: 'wc/v1',
            queryStringAuth: true
        });

        WooCommerce.getAsync('products').then(function (result) {
            setproducts(JSON.parse(result.toJSON().body))
            
        });


        WooCommerce.getAsync('customers?per_page=10').then(function (result) {   
            setCustomers(JSON.parse(result.toJSON().body))
            let obj = {}
            for (const [key, value] of Object.entries(JSON.parse(result.toJSON().body))) {
              
                let objKey = value.id;
                let objValue = `${value.first_name} ${value.last_name} (#${value.username} - ${value.email}) `
                obj[key] = { objKey , objValue }    
                     
                
                if (key == JSON.parse(result.toJSON().body).length - 1) {
                    
                    setCustomerDetail(obj)
                  

                }
            }

        
      
        });

        WooCommerce.getAsync('orders').then(function (result) {
            setOrders(JSON.parse(result.toJSON().body))
        });


    }, []);


    let searchCustomer = (e) =>{

        WooCommerce.getAsync(`customers?search=${e}`).then(function (result) {   
         
            setCustomers(JSON.parse(result.toJSON().body))

            let obj = {}
            for (const [key, value] of Object.entries(JSON.parse(result.toJSON().body))) {
                let objKey = value.id;
                let objValue = `${value.first_name} ${value.last_name} (#${value.username} - ${value.email}) `
                obj[key] = { objKey , objValue }    
                     
                
                if (key == JSON.parse(result.toJSON().body).length - 1) {
                    
                    setCustomerDetail(obj)
                  

                }
            }
        });
    

    }

    let searchProduct = ( e ) =>{
        WooCommerce.getAsync(`products?search=${e}`).then(function (result) {
            setproducts(JSON.parse(result.toJSON().body))
            console.log(JSON.parse(result.toJSON().body))

        });
       

    }

    return (
        <MyContext.Provider
            value={{
                //   state
                products,
                customers,
                orders,
                customerdetail,

                //   Functions
                searchCustomer,
                searchProduct


            }}
        >

            <Router history={hist}>
                <Switch>
                    <Route path="/admin" component={Admin} />
                    <Route path="/rtl" component={RTL} />
                    <Redirect from="/" to="/admin/dashboard" />
                </Switch>
            </Router>


        </MyContext.Provider>
    );
}

export default App;