
import Dashboard from "@material-ui/icons/Dashboard";
import Orders from "@material-ui/icons/Assignment";
import Products from "@material-ui/icons/List";
import DashboardPage from "views/Dashboard/Dashboard.js";
import OrdersPage from "views/Orders/Orders.js";
import ProductsPage from "views/Products/Products.js";
import CreateOrderPage from "views/CreateOrder/CreateOrder.js";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    rtlName: "لوحة القيادة",
    icon: Dashboard,
    component: DashboardPage,
    layout: "/admin"
  },
  {
    path: "/orders",
    name: "Orders",
    rtlName: "لوحة القيادة",
    icon: Orders,
    component: OrdersPage,
    layout: "/admin"
  },
  {
    path: "/products",
    name: "Products",
    rtlName: "لوحة القيادة",
    icon: Products,
    component: ProductsPage,
    layout: "/admin"
  },
  {
    path: "/create-order",
    name: "Create Order",
    rtlName: "لوحة القيادة",
    icon: Orders,
    component: CreateOrderPage,
    layout: "/admin"
  }
];

export default dashboardRoutes;
