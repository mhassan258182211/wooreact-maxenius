import React, { Component } from "react";
import axios from "axios";
import addOAuthInterceptor from "axios-oauth-1.0a";
import { Table, TableHead, TableBody, TableRow, TableCell, Card, Button, Modal, Fade, CardContent, Icon } from "@material-ui/core";
import CardHeader from "@material-ui/core/CardHeader";
import CardBody from "components/Card/CardBody";
import api from '../../api.js'
import MyContext from "../../components/contextApi/context";
import { Router, Route, Switch, Redirect , Link } from "react-router-dom";


export default function Orders() {

  return (
    <MyContext.Consumer>
      {(context) => {

        return (

          <div>
            <Card>
              <CardHeader
                title="Orders"
              action={
                <Link to='/admin/create-order' >
                <Button style={{marginTop:20, marginRight:20}} variant="contained" color="primary">
                  Add Order
                </Button>
                </Link>
              }
              />
              <CardBody>
                <Table>
                  <TableHead>
                    <TableRow>
                      <TableCell>ID</TableCell>
                      <TableCell>Status</TableCell>
                      <TableCell>Total</TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {context.orders && context.orders.map(order => {
                      return (
                        <TableRow>
                          <TableCell>{order.id}</TableCell>
                          <TableCell>{order.status}</TableCell>
                          <TableCell>{order.currency} {order.total}</TableCell>
                        </TableRow>
                      )
                    })
                    }
                  </TableBody>
                </Table>
              </CardBody>
            </Card>
          </div>


        )
      }}

    </MyContext.Consumer>
  )
}

