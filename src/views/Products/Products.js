import React, { Component } from "react";
import axios from "axios";
import addOAuthInterceptor from "axios-oauth-1.0a";
import { Table, TableHead, TableBody, TableRow, TableCell, Card, Button, Modal, Fade, CardContent, Icon } from "@material-ui/core";
import CardHeader from "@material-ui/core/CardHeader";
import CardBody from "components/Card/CardBody";
import CreateProduct from "views/CreateProduct/CreateProduct";
import MyContext from "../../components/contextApi/context";

class Products extends Component {
  constructor() {
    super();
    this.state = {

      open: false,
    }
    this.openModal = this.openModal.bind(this);
    this.closeModal = this.closeModal.bind(this);

  }

  openModal() {
    this.setState({ 'open': true });
  }
  closeModal() {
    this.setState({ 'open': false });
  }


  render() {
    return (
      <MyContext.Consumer>
        {(context) => {
          return (
            <div>
              {/* <button onClick={()=>{context.setName("Manzoor")}} >{context.name}</button> */}

              <Card>
                <CardHeader
                  title="Products"
                // action={
                //   <Button onClick={this.openModal} style={{marginTop:20, marginRight:20}} variant="contained" color="primary">
                //     Create New
                //   </Button>
                // }
                />
                <CardBody>
                  <Table>
                    <TableHead>
                      <TableRow>
                        <TableCell>ID</TableCell>
                        <TableCell>Name</TableCell>
                        <TableCell>Price</TableCell>
                        <TableCell>Actions</TableCell>
                      </TableRow>
                    </TableHead>
                    <TableBody>
                      {context.products && context.products.map(product => {
                        return (
                          <TableRow>
                            <TableCell>{product.id}</TableCell>
                            <TableCell>{product.name}</TableCell>
                            <TableCell>{product.regular_price}</TableCell>
                            <TableCell></TableCell>
                          </TableRow>
                        )
                      })
                      }
                    </TableBody>
                  </Table>
                </CardBody>
              </Card>
              <Modal
                style={{
                  display: "flex",
                  alignItems: "center",
                  justifyContent: "center"
                }}
                aria-labelledby="simple-modal-title"
                aria-describedby="simple-modal-description"
                open={this.state.open}
                onClose={this.closeModal}
              >
                <div>
                  <div style={{
                    backgroundColor: "white",
                    padding: "30px"
                  }}>
                    <CreateProduct onProductCreated={this.getProducts}></CreateProduct>
                  </div>
                </div>
              </Modal>
            </div>
          )
        }}

      </MyContext.Consumer>

    )
  }
}

export default Products
