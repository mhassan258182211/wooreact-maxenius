import React, {Component} from "react";
import axios from "axios";

import addOAuthInterceptor from "axios-oauth-1.0a";
import { Button, Card, CardContent, Grid, TextField } from "@material-ui/core";
import CardHeader from "@material-ui/core/CardHeader";


class  CreateProduct extends Component {
  constructor(){
    super();
    this.state ={
        name: "",
        type: "simple",
        regular_price: "",
        description:"",
        short_description:"",
        categories: [],
        image: [],

    }
    this.createProduct = this.createProduct.bind(this);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeRegularPrice = this.onChangeRegularPrice.bind(this);
    this.onChangeDescription = this.onChangeDescription.bind(this);
    this.onChangeShortDiscription = this.onChangeShortDiscription.bind(this);
  }

  componentDidMount(){
  }
  onChangeName(e){
    this.setState({ name: e.target.value })
  }
  onChangeRegularPrice(e){
    this.setState({ regular_price: e.target.value })
  }
  onChangeDescription(e){
    this.setState({ description: e.target.value })
  }
  onChangeShortDiscription(e){
    this.setState({ short_description: e.target.value })
  }

  async createProduct() {
    const client = axios.create();
    addOAuthInterceptor(client, {
      key: "ck_6c95f1f5f5e765044ec1edbe3fd1709837bc3cac",
      secret: "cs_1d77c90b87e8cc8c32b97ee03154cc3c5f0918fa",
     algorithm: "HMAC-SHA1",
     });
    await client.post('http://localhost/react-word/wp-json/wc/v3/products',this.state)
    .then((response)=>{
      console.log(response.data)
      this.props.onProductCreated()
    })
  }
  render() {
    return(
        <div>
            <Card
            style={{padding:"20px"}}
            >
                <CardHeader
                    title="Create New Product"
                />
                <CardContent>
                    <Grid container>
                        <Grid item xs={12} sm={12} md={4}>
                            <TextField onChange={this.onChangeName} value={this.state.name} style={{padding:"20px"}} required id="standard-required" label="Name" />
                        </Grid>
                        <Grid item xs={12} sm={12} md={4}>
                            <TextField onChange={this.onChangeRegularPrice} value={this.state.regular_price} style={{padding:"20px"}} multiline={true} required id="standard-required" label="Regular Price" />
                        </Grid>
                        <Grid item xs={12} sm={12} md={12}>
                            <TextField onChange={this.onChangeDescription}  value={this.state.description} fullWidth={true} multiline={true} style={{padding:"20px"}} required id="standard-required" label="Description" />
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={12} sm={12} md={12}>
                            <TextField onChange={this.onChangeShortDiscription}  value={this.state.short_description} fullWidth={true} style={{padding:"20px"}} required id="standard-required" label="Short Description" />
                        </Grid>
                    </Grid>
                    <Grid container>
                        <Grid item xs={12} sm={12} md={12}>
                            <Button variant="contained" color="primary" onClick={this.createProduct}>Save Product</Button>
                        </Grid>
                    </Grid>
                </CardContent>
            </Card>
        </div>
      
    )
  }
}

export default CreateProduct
