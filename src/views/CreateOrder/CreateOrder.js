// import React, {Component} fsrom "react";
import React, { useReducer, createContext, useContext, useState, Component } from 'react';

import { Button, Card, CardContent, FormControl, Grid, InputLabel, MenuItem, Select, Table, TableBody, TableCell, TableHead, TableRow, TextField, Typography } from "@material-ui/core";
import CardHeader from "@material-ui/core/CardHeader";
import api from '../../api.js'
import Autocomplete from '@material-ui/lab/Autocomplete';
import MyContext from "../../components/contextApi/context";
import CustomInput from 'components/CustomInput/CustomInput.js';
import SelectSearch from 'react-select-search';


class CreateOrder extends Component {
    constructor() {
        super();
        this.state = {
            name: null,
            productId: null,
            quantity: 1,
            customers: [],
            products: [],
            customerId: null,
            items: [],
            b_f_name: null,
            b_l_name: null,
            b_a_l_1: null,
            b_a_l_2: null,
            b_city: null,
            b_state: null,
            b_post_code: null,
            b_country: null,
            b_email: null,
            b_phone: null,
            b_company: null,
            s_f_name: null,
            s_l_name: null,
            s_a_l_1: null,
            s_a_l_2: null,
            s_city: null,
            s_state: null,
            s_post_code: null,
            s_country: null,
            s_company: null,
            flag2: true,
            flag3: true,


            shipping_charges: 0,
            payment: "true",
            flag: true
        }
        this.selectCustomer = this.selectCustomer.bind(this);
        this.selectQuantity = this.selectQuantity.bind(this);
        this.selectProduct = this.selectProduct.bind(this);
        this.addItem = this.addItem.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.cOrder = this.cOrder.bind(this);
    }



    async componentDidMount() {

    }
    handleChange(e, name) {
        this.setState({ [name]: e.target.value });
        
    };
    selectCustomer(e) {
        if (e) {
            this.setState({
                customerId: e.objKey
            });

        }

    }
    selectQuantity(e) {
        this.setState({
            quantity: e.target.value
        });
    }
    cOrder() {

        let set = {
            'payment_method': 'bacs',
            'payment_method_title': 'Direct Bank Transfer',
            'set_paid': (this.state.payment) ? this.state.payment : "",
            'customer_id': (this.state.customerId) ? this.state.customerId : "",
            'billing': {
                'first_name': (this.state.b_f_name) ? this.state.b_f_name : "",
                'last_name': (this.state.b_l_name) ? this.state.b_l_name : "",
                'address_1': (this.state.b_a_l_1) ? this.state.b_a_l_1 : "",
                'address_2': (this.state.b_a_l_2) ? this.state.b_a_l_2 : "",
                'city': (this.state.b_city) ? this.state.b_city : "",
                'state': (this.state.b_state) ? this.state.b_state : "",
                'postcode': (this.state.b_post_code) ? this.state.b_post_code : "",
                'country': (this.state.b_country) ? this.state.b_country : "",
                'email': (this.state.b_email) ? this.state.b_email : "example@example.com",
                'phone': (this.state.b_phone) ? this.state.b_phone : "",
                'company': (this.state.b_company) ? this.state.b_company : "",

            },
            'shipping': {
                'first_name': (this.state.s_f_name) ? this.state.s_f_name : "",
                'last_name': (this.state.s_l_name) ? this.state.s_l_name : "",
                'address_1': (this.state.s_a_l_1) ? this.state.s_a_l_1 : "",
                'address_2': (this.state.s_a_l_2) ? this.state.s_a_l_2 : "",
                'city': (this.state.s_city) ? this.state.s_city : "",
                'state': (this.state.s_state) ? this.state.s_state : "",
                'postcode': (this.state.s_post_code) ? this.state.s_post_code : "",
                'country': (this.state.s_country) ? this.state.s_country : "",
                'company': (this.state.s_company) ? this.state.s_company : "",
            },
            'line_items': (this.state.items) ? this.state.items : "",
            'shipping_lines': [
                {
                    'method_id': 'flat_rate',
                    'method_title': 'Flat Rate',
                    'total': (this.state.shipping_charges) ? this.state.shipping_charges : ""
                }
            ]
        };


        


    }
    addItem() {
        let old = false
        let items = this.state.items

        items.map((item) => {
            if (item.product_id === this.state.productId) {
                item.quantity += this.state.quantity
                old = true
            }
        })
        if (!old) {
            this.setState({
                items: [...this.state.items, {
                    product_id: this.state.productId,
                    quantity: this.state.quantity,
                    name: this.state.name
                }
                ]
            })
        } else {
            this.setState({
                items: items
            })
        }
    }
    selectProduct(event, value) {
        if (value) {
            this.setState({
                productId: value.id,
                name: value.name
            });
        }
    }
    render() {
        const { customerId, products, productId, quantity, items, payment } = this.state;


        let value;
        let valueProduct;

      

        return (

            <MyContext.Consumer>
                {(context) => {
                    if (this.state.flag) {
                        this.setState({ products: context.products })
                        this.setState({ flag: false })
                    }
                    return (
                        <div>



                            <Card
                                style={{ padding: "10px" }}>
                                <CardHeader
                                    title="Create New Order"
                                />
                                <CardContent>
                                    <div className="columnMain">
                                        <div className="col-A" >

                                            <Typography style={{ marginTop: '20px', }}>General </Typography>
                                            <InputLabel id="demo-simple-select-label" style={{ width: '100%', textAlign: 'left', color: "black" }} >Customer</InputLabel>
                                            <FormControl style={{
                                                margin: 10,
                                                width: '100%',
                                                float: "center"
                                            }}>



                                                <Grid style={{ textAlign: "center", display: "flex", justifyContent: "center" }}>

                                                    <Autocomplete
                                                        id="combo-box-demo"
                                                        options={
                                                            Object.entries(context.customerdetail).map((element, index) => {
                                                                return (
                                                                    element[1]
                                                                )

                                                            })
                                                        }
                                                        getOptionLabel={(option) => option.objValue}
                                                        style={{ width: "90%" }}
                                                        renderInput={(params) => {

                                                            if (params.inputProps.value) {
                                                                if (value || this.state.flag2) {
                                                                    context.searchCustomer(params.inputProps.value)
                                                                    if (this.state.flag2) {
                                                                        this.setState({ flag2: false })
                                                                    }

                                                                }
                                                                value = params.inputProps.value

                                                                // console.log(params.inputProps.value, value)

                                                            }

                                                            return <TextField {...params} label="Customer" variant="outlined" />
                                                        }}

                                                        onChange={(event, newValue) => { this.selectCustomer(newValue) }}

                                                    />

                                                </Grid>
                                            </FormControl>


                                            <Grid style={{ textAlign: "center" }}  >
                                                <Typography style={{ textAlign: "left", marginTop: "5%", width: "80%" }}>Shipping Charges </Typography>
                                                <TextField onChange={(e) => this.handleChange(e, 'shipping_charges')} style={{ margin: '0px 10px', width: '90%' }} id="standard-basic" label="Shipping Charges" />
                                            </Grid>


                                            <Grid style={{ textAlign: 'left' }} >
                                                <Typography style={{ textAlign: 'left', marginTop: "9%" }}>Payment </Typography>
                                                <Select
                                                    value={payment}
                                                    labelId="demo-simple-select-label"
                                                    id="demo-simple-select"
                                                    style={{ margin: '10px', width: '90%' }}
                                                    onChange={(e) => this.handleChange(e, 'payment')}
                                                    variant="outlined"
                                                >
                                                    <MenuItem value="true">Paid</MenuItem>
                                                    <MenuItem value="false">Pending</MenuItem>

                                                </Select>
                                            </Grid>



                                        </div>
                                        <div className="col-B" >
                                            <Typography style={{ marginTop: '20px' }}>Billing</Typography>
                                            <Grid>
                                                <TextField onChange={(e) => this.handleChange(e, 'b_f_name')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="First Name" />
                                                <TextField onChange={(e) => this.handleChange(e, 'b_l_name')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="Last Name" />
                                                <TextField onChange={(e) => this.handleChange(e, 'b_a_l_1')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="Address Lin 1" />
                                                <TextField onChange={(e) => this.handleChange(e, 'b_a_l_2')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="Address Line 2" />
                                                <TextField onChange={(e) => this.handleChange(e, 'b_city')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="City" />
                                                <TextField onChange={(e) => this.handleChange(e, 'b_state')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="State" />
                                                <TextField onChange={(e) => this.handleChange(e, 'b_post_code')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="Post Code" />
                                                <TextField onChange={(e) => this.handleChange(e, 'b_country')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="Country" />
                                                <TextField onChange={(e) => this.handleChange(e, 'b_email')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="Email" />
                                                <TextField onChange={(e) => this.handleChange(e, 'b_phone')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="Phone" />
                                                <TextField onChange={(e) => this.handleChange(e, 'b_company')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="Company" />
                                            </Grid>

                                        </div>
                                        <div className="col-C" >
                                            <Typography style={{ marginTop: '20px' }}>Shipping </Typography>

                                            <Grid>
                                                <TextField onChange={(e) => this.handleChange(e, 's_f_name')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="First Name" />
                                                <TextField onChange={(e) => this.handleChange(e, 's_l_name')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="Last Name" />
                                                <TextField onChange={(e) => this.handleChange(e, 's_a_l_1')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="Address Lin 1" />
                                                <TextField onChange={(e) => this.handleChange(e, 's_a_l_2')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="Address Line 2" />
                                                <TextField onChange={(e) => this.handleChange(e, 's_city')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="City" />
                                                <TextField onChange={(e) => this.handleChange(e, 's_state')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="State" />
                                                <TextField onChange={(e) => this.handleChange(e, 's_post_code')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="Post Code" />
                                                <TextField onChange={(e) => this.handleChange(e, 's_country')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="Country" />
                                                <TextField onChange={(e) => this.handleChange(e, 's_company')} style={{ margin: '10px', width: '80%' }} id="standard-basic" label="Company" />
                                            </Grid>

                                        </div>


                                    </div>
                                    <CardHeader
                                        title="Add Itms"
                                    />

                                    <Grid  >
                                       
                                            <Grid style={{ display: 'flex' }} >

                                                <Autocomplete
                                                    id="combo-box-demo"
                                                    style={{ width: "35%" }}
                                                    options={context.products}

                                                    onChange={this.selectProduct}
                                                    getOptionLabel={(option) => option.name}
                                                    renderOption={(option) => (
                                                        <React.Fragment>
                                                            {option.name}
                                                        </React.Fragment>
                                                    )}
                                                    renderInput={(params) => {

                                                        if (params.inputProps.value) {
                                                            if (valueProduct || this.state.flag3) {
                                                                context.searchProduct(params.inputProps.value)
                                                                if (this.state.flag3) {
                                                                    this.setState({ flag3: false })
                                                                }

                                                            }
                                                            valueProduct = params.inputProps.value

                           

                                                        }




                                                        return <TextField
                                                            {...params}
                                                            label="Items"
                                                            inputProps={{
                                                                ...params.inputProps,
                                                            }}
                                                            variant="outlined"
                                                        />



                                                    }} />
                                                <Select
                                                    labelId="quantity-label"
                                                    value={quantity}
                                                    style={{ marginLeft: "20px", width: "10%" }}
                                                    id="demo-simple-select"
                                                    onChange={this.selectQuantity}
                                                    variant="outlined"
                                                >
                                                    <MenuItem value={1}>1</MenuItem>
                                                    <MenuItem value={2}>2</MenuItem>
                                                    <MenuItem value={3}>3</MenuItem>
                                                    <MenuItem value={4}>4</MenuItem>
                                                    <MenuItem value={5}>5</MenuItem>

                                                </Select>
                                                <Button onClick={this.addItem} style={{ marginLeft: '20px', marginTop: '10px', height: '10%' }} color="primary" variant="contained">Add Item</Button>
                                            </Grid>
                                        
                                    </Grid>
                                    <Grid item xs={12} sm={12} md={12}>
                                        <Table>
                                            <TableHead>
                                                <TableRow>
                                                    <TableCell>ID</TableCell>
                                                    <TableCell>Name</TableCell>
                                                    <TableCell>Quantity</TableCell>
                                                </TableRow>
                                            </TableHead>
                                            <TableBody>
                                                {this.state.items && this.state.items.map(item => {
                                                    return (
                                                        <TableRow>
                                                            <TableCell>{item.product_id}</TableCell>
                                                            <TableCell>{item.name}</TableCell>
                                                            <TableCell>{item.quantity}</TableCell>
                                                        </TableRow>
                                                    )
                                                })}

                                            </TableBody>
                                        </Table>
                                    </Grid>
                                    <Grid style={{ display: 'flex', justifyContent: "center", margin: '3%' }} >
                                        <Button onClick={this.cOrder} style={{ marginTop: '20px', marginLeft: '20px' }} color="primary" variant="contained">Place Order</Button>
                                    </Grid>
                                </CardContent>
                            </Card>
                        </div>

                    )
                }}

            </MyContext.Consumer>

        )
    }
}

export default CreateOrder
